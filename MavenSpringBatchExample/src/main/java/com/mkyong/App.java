package com.mkyong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	private static Logger logger = LogManager.getLogger(App.class.getName());


	public static void main(String[] args) {

		App obj = new App();
		obj.run();

	}

	private void run() {

		String[] springConfig = { "spring/batch/jobs/job-read-files.xml" };

		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);

		logger.info("Load context successfully ..." + context.getApplicationName());
		
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job job = (Job) context.getBean("readMultiFileJob");

		try {

			JobExecution execution = jobLauncher.run(job, new JobParameters());
			logger.info("Exit Status ..." + execution.getStatus());
			logger.info("Exit Status ..." + execution.getAllFailureExceptions());

		} catch (Exception e) {
			e.printStackTrace();

		}

		logger.info("Job Processed status [DONE]");

	}

}
