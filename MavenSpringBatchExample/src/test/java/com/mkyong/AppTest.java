package com.mkyong;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>AppTest</code> contains tests for the class <code>{@link App}</code>.
 *
 * @generatedBy CodePro at 2/24/15 2:04 PM
 * @author abouba
 * @version $Revision: 1.0 $
 */
public class AppTest {
	/**
	 * Run the App() constructor test.
	 *
	 * @generatedBy CodePro at 2/24/15 2:04 PM
	 */
	@Test
	public void testApp_1()
		throws Exception {
		App result = new App();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 2/24/15 2:04 PM
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		App.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'jobRepository' defined in class path resource [spring/batch/config/context.xml]: Invocation of init method failed; nested exception is java.lang.IllegalArgumentException: interface org.springframework.aop.SpringProxy is not visible from class loader
		//       at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.initializeBean(AbstractAutowireCapableBeanFactory.java:1482)
		//       at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:521)
		//       at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:458)
		//       at org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:295)
		//       at org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:223)
		//       at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:292)
		//       at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:194)
		//       at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:608)
		//       at org.springframework.context.support.AbstractApplicationContext.finishBeanFactoryInitialization(AbstractApplicationContext.java:932)
		//       at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:479)
		//       at org.springframework.context.support.ClassPathXmlApplicationContext.<init>(ClassPathXmlApplicationContext.java:139)
		//       at org.springframework.context.support.ClassPathXmlApplicationContext.<init>(ClassPathXmlApplicationContext.java:93)
		//       at com.mkyong.App.run(App.java:27)
		//       at com.mkyong.App.main(App.java:19)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 2/24/15 2:04 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 2/24/15 2:04 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 2/24/15 2:04 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(AppTest.class);
	}
}